# Amazon Prime IMDb

This is a firefox browser extension which injects the respective IMDb rating score to a movie when hovering over it. Its was developed for learning purposes so please do not take it to seriously ;-)

Currently only tested on:
- Firefox 91.0.1 (64-Bit)
- german amazon website

## Setup

1. type in `about:debugging#/runtime/this-firefox`
2. load this extension

## Use

1. goto amazon
2. navigate to "Mein Konto" -> "Mein Prime Video"
3. hover over a movie card and the respective IMDb rating will be showed
4. optionally click on the IMDb logo to open a tab with the respective IMDb url

## License

Copyright © 2021 [Benjamin Heinzerling](https://bitbucket.org/bheinzerling/amazon_prime_imdb). <br />
This project is [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html) licensed.
