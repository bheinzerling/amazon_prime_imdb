// change browser action icon accordingly to activity
browser.runtime.onMessage.addListener((request, sender, sendResponse) => 
{
    console.debug({request, sender, sendResponse})
    //if(sender.envType === "content_child")
    if(request.active)
        browser.browserAction.setIcon({path: "resources/icons/icon.ico"})
    else
        browser.browserAction.setIcon({path: "resources/icons/icon_grey.ico"})
})
