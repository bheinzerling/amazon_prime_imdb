let ratingCssPath = undefined

/**
 * scrapes imdb site for movie rating 
 * @param {String} title The title of the movie
 * @returns 
 */
const imdbRating = async (title) => {
    try
    {
        // return value
        const ret = {
            rating: undefined,
            searchMovieEndpoint: undefined,  // first link
            ratingEndpoint: undefined  // second link
        }

        // imdb search title endpoint
        ret.searchMovieEndpoint = URL_.imdbUrl + URL_.imdbParams + title.replaceAll(" ", "+")

        // fetch searchMovie endpoint
        let response = await fetch(ret.searchMovieEndpoint, { headers: { "Accept": ["text/html", "text/plain"] }, })
        if (!response.ok) 
            throw Error("fetch error")
        let responseHtml = await response.text()

        // scrape movie link to click 
        const parser = new DOMParser()
        const searchMovieDOM = parser.parseFromString(responseHtml, "text/html")
        const movieLink = searchMovieDOM.querySelector(SEL_IMDB.movieLink)
        if (!movieLink) 
            return ret

        // click movie link
        ret.ratingEndpoint = URL_.imdbUrl + movieLink.getAttribute("href")
        response = await fetch(ret.ratingEndpoint, { header: { "Accept": ["text/html", "text/plain"] }, })
        if (!response.ok) 
            throw Error("fetch error")
        responseHtml = await response.text()

        // find appropriate rating css path
        if(!ratingCssPath)
        {
            SEL_IMDB.ratingList.forEach(cssPath => {
                const ratingDoc = parser.parseFromString(responseHtml, "text/html")
                ratingElem = ratingDoc.querySelector(cssPath)
                if(ratingElem)
                {
                    ratingCssPath = cssPath
                    console.debug("found css path for rating: " + cssPath)
                }
            })
        }

        // scrape rating
        if(ratingCssPath)
        {
            const ratingDoc = parser.parseFromString(responseHtml, "text/html")
            ratingElem = ratingDoc.querySelector(ratingCssPath)
            if(ratingElem)
                ret.rating = ratingElem.innerHTML
        }

        return ret
    }
    catch(e)
    {
        console.trace(e)
    }
}