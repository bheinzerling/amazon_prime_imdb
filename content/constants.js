const URL_ =
{
    imdbUrl: "https://www.imdb.com/",
    imdbParams: "find?q=",
}

const SEL_AMAZ =
{
    // row has one rowUl has cards has cardTitles
    row: "div#aiv-cl-main-middle > div > div > div > div.u-collection.tst-collection",
    rowUl: "div.tst-faceted-carousel > div > div > div > ul",
    card: " .tst-card-wrapper.tst-hoverable-card",
    cardTitle: " .tst-hover-title",
}

const SEL_IMDB =
{
    movieLink: "table.findList tr.findResult.odd td.result_text a",

    // first one that works will be used
    ratingList: [
    "span[class^=\"AggregateRatingButton__RatingScore\"]",
    "div.imdbRating span[itemprob=\"ratingValue\"]",
    "div.imdbRating span",
    ]
}

const CONSTANTS =
{
    HOVER_TIME_MS: 500  // you have to hover this long over a movie card element before the process starts
}