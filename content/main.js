let CARDS = []

/**
 * search for new card elements
 */
const findCardElems = () =>
{
    const cardElems = document.querySelectorAll(SEL_AMAZ.card)
    console.debug("found cards: " + cardElems.length)

    return cardElems
}

const listenToCardElems = (cardElems) =>
{
    cardElems.forEach(card => {
        if(!(card in CARDS))
        {
            CARDS.push(card)
            card.addEventListener("mouseenter", event => { injectRatingToCardElem(event) })
        }
    })
}

/**
 * checks whether card element already has rating injected 
 * @param {*} card 
 * @returns 
 */
const cardElemHasImdbRating = (card) =>
{
    if(card.querySelector(" .rating_element"))
        return true;
    else
        return false;
}

/**
 * manipulates movie title for better search results 
 * @param {*} title 
 */
const trimMovieTitle = (title) =>
{
    if(typeof(title) !== "string")
        throw Error("wrong type: " + typeof(sel))

    // whitelist
    const matches = title.match(/[a-zA-Z0-9 ,.-:]+/)
    if(matches)
        return matches[0]
    else
        return null
}

/**
 * scrapes imdb for rating and injects it to card element 
 * @param {*} event 
 * @returns 
 */
const injectRatingToCardElem = event =>
{
    try
    {
        // card element already has an imdb rating element injected
        if(cardElemHasImdbRating(event.target))
            return

        // delete previous run if still active
        if(typeof(injectRatingToCardElem._timeoutId) != "undefined")
        {
            clearTimeout(injectRatingToCardElem._timeoutId)
        }

        injectRatingToCardElem._timeoutId = setTimeout(async () => 
        {
            // get movie title
            const titleElement = event.target.querySelector(SEL_AMAZ.cardTitle)
            console.debug("title from card element: " + titleElement.innerHTML)

            // build rating element
            const ratingElem = document.createElement("div")
            ratingElem.className += "rating_element"
            ratingElem.innerHTML = ` <a class="rating_element__imdb_logo">IMDb</a> <div class="loading-sign"></div> `

            // append rating element
            const parentElement = titleElement.parentNode
            parentElement.appendChild(ratingElem)

            // scrape imdb for rating and update rating element accordingly
            const ratingObj = await imdbRating(trimMovieTitle(titleElement.innerHTML))
            console.debug({ratingObj})
            if(ratingObj)
            {
                if(ratingObj.rating)
                {
                    // replace loading sign with rating
                    ratingElem.innerHTML = ` <a href="${ratingObj.ratingEndpoint}" target="_blank" rel="norefferer noopener" class="rating_element__imdb_logo">IMDb</a> ${ratingObj.rating}
                        `

                    // additional small rating element in top right corner
                    const ratingElemSmall = document.createElement("div")
                    ratingElemSmall.className += "rating_element_small"
                    ratingElemSmall.innerHTML = `${ratingObj.rating}`
                    event.target.appendChild(ratingElemSmall)

                    return
                }
                else  // ratingObj but no rating
                    ratingElem.innerHTML = ` <a href="${ratingObj.searchMovieEndpoint}" target="_blank" rel="norefferer noopener" class="rating_element__imdb_logo">IMDb</a> ;-( `
            }
            
            // no ratingObj
            ratingElem.innerHTML = ` <a target="_blank" rel="norefferer noopener" class="rating_element__imdb_logo">IMDb</a> ;-( `
        }, CONSTANTS.HOVER_TIME_MS)
    }
    catch(e)
    {
        console.trace(e)
    }
}

/**
 * scrapes classes from css selector
 * @param {*} sel 
 */
const getClassListFromSel = (sel) =>
{
    if(typeof(sel) !== "string")
        throw Error("wrong type: " + typeof(sel))

    let matches = sel.match(/\.[a-zA-Z_\-]+/g)
    if(matches)
        matches = matches.map((match) => {
            return match.slice(1)  // remove leading "."
        })
    return matches
}

/**
 * search for certain elements and add certain event listeners to them
 * @returns 
 */
const main = () =>
{
    try
    {
        console.debug("%cI GOT LOADED", "color: green; font-weight: bold")
        window.addEventListener("pagehide", e => 
        {
            browser.runtime.sendMessage({active: false})
        });

        // search for parent div which contains rows
        const firstRow = document.querySelector(SEL_AMAZ.row)
        if(!firstRow)
        {
            browser.runtime.sendMessage({active: false})
            console.debug("can't find root element. returning")
            return
        }
        browser.runtime.sendMessage({active: true})

        const rootElem = firstRow.parentNode
        console.debug({rowContainerElem: rootElem})

        const rowClasses = getClassListFromSel(SEL_AMAZ.row)
        const cardClasses = getClassListFromSel(SEL_AMAZ.card)

        // observer
        const observer = new MutationObserver((mutations, observer) => {
            mutations.forEach(function (mutation) {
                try{
                    console.debug({mutation})
                    if(mutation.type === "childList")  // new nodes added
                    {
                        mutation.addedNodes.forEach(node => {
                            if("classList" in node)
                            {
                                // row 
                                console.debug(node.classList)
                                if(rowClasses.every(class_ => { return node.classList.contains(class_) }))  // classList -> domtokenlist -> contains
                                {
                                    console.debug("found row")

                                    // find ul in row which contains cards and observe it
                                    const rowUl = node.querySelector(SEL_AMAZ.rowUl)
                                    if(rowUl)
                                    {
                                        console.debug("found rowUl")
                                        observer.observe(rowUl, { childList: true, subtree: false})

                                        // find all cards in ul
                                        const cards = rowUl.querySelectorAll(SEL_AMAZ.card)

                                        // listen to card events
                                        listenToCardElems(cards)
                                    }
                                }
                                // card 
                                else if(cardClasses.every(class_ => { return node.classList.contains(class_) }))
                                {
                                    console.debug("found card")
                                    listenToCardElems([node])
                                }
                                // something unknown added? has it cards? example: some rows have multiple cards per li
                                else
                                {
                                    console.debug("found element in rowUl")

                                    // find all cards in ul
                                    const cards = node.querySelectorAll(SEL_AMAZ.card)

                                    if(cards)
                                    {
                                        // listen to card events
                                        listenToCardElems(cards)
                                    }
                                }
                            }
                        })
                    }
                }
                catch(e)
                {
                    console.trace(e)
                }
            });
        });

        // init observer
        // find all rows
        const rows = rootElem.querySelectorAll(SEL_AMAZ.row)
        rows.forEach(row => {
            // find ul in row
            const ul = row.querySelector(SEL_AMAZ.rowUl)

            if(ul)
            {
                // observe ul
                observer.observe(ul, { childList: true, subtree: false })

                // find all cards in ul
                const cards = ul.querySelectorAll(SEL_AMAZ.card)

                if(cards)
                {
                    // listen to card events
                    listenToCardElems(cards)
                }
            }
        })

        // observe root element for added rows
        observer.observe(rootElem, { childList: true, subtree: false })
    }
    catch(e)
    {
        console.trace(e)
    }
}

main()
